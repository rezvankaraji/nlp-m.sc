import re
import hazm
import random
from jiwer import wer
from math import log10


class language_model:
    def prob(self):
        # find most probable unigram token
        if self.n == 1:
            total = 0
            for token in self.uni_freq.keys():
                total = total + self.uni_freq[token]
            MAX = 0
            for token in self.uni_freq.keys():
                p = -log10(self.uni_freq[token] / total)
                if MAX < p:
                    MAX = p
                    self.uni_most_freq = (token, p)
        # find most probable bigram tokens
        if self.n == 2:
            total = {}
            for token in self.bi_freq.keys():
                if token[0] not in total.keys():
                    total[token[0]] = 0
                total[token[0]] = total[token[0]] + self.bi_freq[token]
            MAX = {}
            next_token = {}
            for token in self.bi_freq.keys():
                p = -log10(self.bi_freq[token] / total[token[0]])
                if token[0] not in MAX.keys():
                    MAX[token[0]] = 0
                if MAX[token[0]] < p:
                    MAX[token[0]] = p
                    next_token[token[0]] = token[1]
            for x in next_token.keys():
                self.bi_most_freq[x] = (next_token[x], MAX[x])
        # find most probable bigram tokens
        if self.n == 3:
            total = {}
            for token in self.tri_freq.keys():
                t = (token[0], token[1])
                if t not in total.keys():
                    total[t] = 0
                total[t] = total[t] + self.tri_freq[token]
            MAX = {}
            next_token = {}
            for token in self.tri_freq.keys():
                t = (token[0], token[1])
                p = -log10(self.tri_freq[token] / total[t])
                if t not in MAX.keys():
                    MAX[t] = 0
                if MAX[t] < p:
                    MAX[t] = p
                    next_token[t] = token[2]
            for x in next_token.keys():
                self.tri_most_freq[x] = (next_token[x], MAX[x])

    # devide data into tokens
    def tokenize(self, sentences):
        tokenized_sentences = []
        for sentence in sentences:
            words = hazm.word_tokenize(sentence)
            for word in words:
                word = re.sub(r"[n]", "N", word)
            tokenized_sentences.append(words)
        return tokenized_sentences

    # join tokens to make n_grams
    def devide_ngrams(self, sentences, n):
        # zip tokens into ngrams
        n_grams_list = []
        for sentence in sentences:
            n_grams = zip(*[sentence[i:] for i in range(n)])
            l = []
            for x in n_grams:
                l.append(x)
            n_grams_list.append(l)
        return n_grams_list

    # calculate frequencies
    def calc_freq(self, n_grams):
        freq = {}
        for sentence in n_grams:
            for n_gram in sentence:
                if n_gram not in freq:
                    freq[n_gram] = 1
                else:
                    freq[n_gram] = freq[n_gram] + 1
        return freq

    def calc_ngrams(self, tokenized_sentences):
        uni_grams = tokenized_sentences
        self.uni_freq = self.calc_freq(uni_grams)

        # bigram
        if self.n > 1:
            for sentence in tokenized_sentences:
                sentence.insert(0, "<s>")
                sentence.append("</s>")
            bi_grams = self.devide_ngrams(tokenized_sentences, 2)
            # calc frequency for each n_gram
            self.bi_freq = self.calc_freq(bi_grams)

            # trigram
            if self.n == 3:
                for sentence in tokenized_sentences:
                    sentence.insert(0, "<s>")
                    sentence.append("</s>")
                tri_grams = self.devide_ngrams(tokenized_sentences, 3)
                # calc frequency for each n_gram
                self.tri_freq = self.calc_freq(tri_grams)

    def train(self, sentences):
        # process data to make n_grams
        tokenized_sentences = self.tokenize(sentences)
        # obtain ngrams from tokens and calc their frequency
        self.calc_ngrams(tokenized_sentences)
        # smoothing
        if self.smoothing == "laplace":
            for x in self.uni_freq:
                self.uni_freq[x] = self.uni_freq[x] + 1
            if not self.bi_freq == {}:
                for x in self.bi_freq:
                    self.bi_freq[x] = self.bi_freq[x] + 1
            if not self.tri_freq == {}:
                for x in self.tri_freq:
                    self.tri_freq[x] = self.tri_freq[x] + 1
        elif self.smoothing == "kneser_ney":
            pass
        # calc probabilities
        self.prob()

    def __init__(self, n, smoothing, corpus_dir):
        self.n = n
        self.smoothing = smoothing
        self.corpus_dir = corpus_dir
        self.uni_freq = {}
        self.bi_freq = {}
        self.tri_freq = {}
        self.uni_most_freq = ()  # one member
        self.bi_most_freq = {}  # one member for each distinct token
        self.tri_most_freq = {}  # one member for each distinst (token1, token2)

        # read train data from processed file
        train_data = ""
        with open(self.corpus_dir, "r", encoding="utf-8") as train_dir:
            train_data = train_dir.read().split("\n")

        # call train
        self.train(train_data)

    # split text into sentences and delete empty sentences.
    def sent_tokenizer(self, text):
        text = re.split("[.?]+", text)
        for x in text:
            if re.fullmatch(r"[^\S\n\t]+", x):
                text.remove(x)
        # strip sentences, so no extra space is at the begining or ending of them.
        sentences = []
        for x in text:
            sentences.append(x.strip())
        return sentences

    # returns the tuple (the most probabe next word, its probability) probbability = -log(p)
    def generate(self, text):
        # process text to make n_grams
        sentences = self.sent_tokenizer(text)
        tokenized_sentences = self.tokenize(sentences)
        next_token = ()

        # unigram
        if self.n == 1:
            next_token = self.uni_most_freq
        # bigram
        elif self.n == 2:
            tokenized_sentences[-1].insert(0, "<s>")
            last_token = tokenized_sentences[-1][-1]
            next_token = self.bi_most_freq[last_token]
        # trigram
        elif self.n == 3:
            tokenized_sentences[-1].insert(0, "<s>")
            tokenized_sentences[-1].insert(0, "<s>")
            last_tokens = (tokenized_sentences[-1][-2], tokenized_sentences[-1][-1])
            try:
                next_token = self.tri_most_freq[last_tokens]
            except:
                key = random.choice(list(self.tri_most_freq))
                next_token = self.tri_most_freq[key]
        return next_token[0]

    def evaluate(self, valid_data):
        splited_sentences = self.sent_tokenizer(valid_data)
        sentences = self.tokenize(splited_sentences)
        w_e_r = 0
        for sentence in sentences:

            # shannon visualization method
            count = 1
            limit = min(len(sentence), 25)
            generated_sentence = self.generate("")
            while not (generated_sentence[-1] == "</s>" or count > limit):
                new_word = self.generate(generated_sentence)
                generated_sentence = generated_sentence + " " + new_word
                count = count + 1
            generated_sentence = re.sub(r"[_]", " ", generated_sentence)

            # calculate word error rates
            joined_sentence = ""
            for word in sentence:
                joined_sentence = joined_sentence + " " + word
                if sentence.index(word) > count:
                    break
            s1 = [joined_sentence[1:]]
            s2 = [generated_sentence]
            w_e_r = w_e_r + wer(s1, s2)

        avr_wer = w_e_r / len(sentences)
        return avr_wer


train_address = "your directory/train_sentences.json"
valid_address = "your directory/valid_sentences.json"

with open(valid_address, "r", encoding="utf-8") as valid_dir:
    valid_data = valid_dir.read()

print("train unigram model ...")
language_model_unigram = language_model(1, "laplace", train_address)
print("validate unigram model ...")
avr_wer = language_model_unigram.evaluate(valid_data)
print(
    "In unigram model average word error rate for validation data is: ", end=" ",
)
print(avr_wer)
print("train bigram model ...")
language_model_bigram = language_model(2, "laplace", train_address)
print("validate bigram model ...")
avr_wer = language_model_bigram.evaluate(valid_data)
print(
    "In bigram model average word error rate for validation data is: ", end=" ",
)
print(avr_wer)
print("train trigram model ...")
language_model_trigram = language_model(3, "laplace", train_address)
print("validate trigram model ...")
avr_wer = language_model_trigram.evaluate(valid_data)
print(
    "In trigram model average word error rate for validation data is: ", end=" ",
)
print(avr_wer)

