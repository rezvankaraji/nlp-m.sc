import re
import hazm
import plot
import time


# remove every extra characters except . and ? and persian words and numbers and replace every number in text with 'N'.
def normalizer(text):
    normal_text = re.sub(r"[a-z]", " ", text, flags=re.I)
    normal_text = re.sub(r"[_]", " ", normal_text)
    normal_text = re.sub(r"[^\w\s . ?]", " ", normal_text)
    normal_text = re.sub(r"[1234567890۱۲۳۴۵۶۷۸۹۰]+", "N", normal_text)
    return normal_text


# calc number of . and ?
def dot_qsn(text):
    text = re.split("[^.?]+", text)
    num_dot = 0
    num_qsn = 0
    for x in text:
        if re.match("[.]+", x):
            num_dot = num_dot + 1
        elif re.match("[?]+", x):
            num_qsn = num_qsn + 1
    return num_dot, num_qsn


# tokenize text to words
def word_tokenizer(text):
    text = re.sub(r"[.?]", " ", text)
    words = hazm.word_tokenize(text)
    for x in words:
        x = re.sub(r"[n]", "N", x)
    return words


# split text into sentences and delete empty sentences.
def sent_tokenizer(text):
    text = re.split("[.?]+", text)
    for x in text:
        if re.fullmatch(r"[^\S\n\t]+", x):
            text.remove(x)
    # strip sentences, so no extra space is at the begining or ending of them.
    sentences = []
    for x in text:
        sentences.append(x.strip())
    return sentences


# calculate frequencies for every word in text.
def calc_freq(words):
    words_freq = {}
    seen_word = set()
    for word in words:
        if word not in seen_word:
            words_freq[word] = 1
            seen_word.add(word)
        else:
            words_freq[word] += 1
    return words_freq


def text_processor(address):

    # open files to read text from and close it.
    with open(address, "r", encoding="utf-8") as src:
        text = src.read()

    # normalize text
    text = normalizer(text)

    # calc number of . and ?
    num_dot, num_qsn = dot_qsn(text)
    print("number of dots and question marks:", end="  ")
    print(num_dot, end=" ")
    print(num_qsn)

    # split text into sentences
    sentences = sent_tokenizer(text)

    # number of sentences in the text
    num_sentences = len(sentences)
    print("number of sentences:", end="  ")
    print(num_sentences)

    # split text into words.
    words = word_tokenizer(text)

    # number of all words
    num_total_words = len(words)
    print("number of all words:", end="  ")
    print(num_total_words)

    # calculate frequencies for every word in text.
    words_freq = calc_freq(words)

    # number of distinct words.
    num_distinct_words = len(words_freq)
    print("number of distinct words:", end="  ")
    print(num_distinct_words)

    return sentences, words_freq, num_total_words


# list n most frequent words in most_frequent.txt
def top_n_frequents(words_freq, num_total_words, n):

    # sort frequencies
    sorted_words_freq = sorted(words_freq, key=words_freq.get, reverse=True)
    sorted_words_freq = set(sorted_words_freq[:n])

    address = "/home/rezvan/nlp-m.sc/corpus/first_corpus/most_frequent.txt"
    dst = open(address, "w", encoding="utf-8")
    for word in sorted_words_freq:
        dst.write(word)
        dst.write("\n")
    dst.close()
    return sorted_words_freq


# calc frequent words coverage percentile
def coverage_percent(sorted_words_freq, words_freq, num_total_words):
    # frequent_percent
    sum = 0

    for word in words_freq:
        if word in sorted_words_freq:
            sum += words_freq[word]

    percentile = (sum / num_total_words) * 100
    print(percentile)
    return percentile


# replace non-frequent words with UNK
def replace_nonfrequent(sentences, sorted_words_freq):
    replace_sentences = []
    for sentence in sentences:
        sentence = re.sub(
            r"[^\S\n\t]+", " ", sentence
        )  # delete extra spaces in middle of the sentence.
        temp = sentence.split(" ")
        sentence = ""
        for x in temp:
            if not x == "N":
                if x not in sorted_words_freq:
                    x = "UNK"
            sentence = sentence + " " + x
        replace_sentences.append(sentence)

    return replace_sentences


def finale_sentences(sentences, address):
    # open file to write the sentences in and close it.
    with open(address, "w", encoding="utf-8") as dst:
        for sentence in sentences:
            dst.write(sentence)
            dst.write("\n")


start = time.time()
address = "/home/rezvan/nlp-m.sc/corpus/first_corpus"
print("processing train set...")
train_normalized_sentences, train_word_freq, train_total_words = text_processor(
    address + "/train.json"
)
print("processing valid set...")
valid_normalized_sentences, valid_word_freq, valid_total_words = text_processor(
    address + "/valid.json"
)

# list 10000 most frequent words
sorted_words_freq = top_n_frequents(train_word_freq, train_total_words, 10000)

# plot for section
plot.plot_power_law(train_word_freq, 5000)

print("word coverage percentile for train set is:", end="  ")
train_percentile = coverage_percent(
    sorted_words_freq, train_word_freq, train_total_words
)

print("word coverage percentile for valid set is:", end="  ")
train_percentile = coverage_percent(
    sorted_words_freq, valid_word_freq, valid_total_words
)

# replace non-frequent words with UNK
train_final_sentences = replace_nonfrequent(
    train_normalized_sentences, sorted_words_freq
)

valid_final_sentences = replace_nonfrequent(
    valid_normalized_sentences, sorted_words_freq
)

print("prepare train sentences file...")
finale_sentences(train_final_sentences, address + "/train_sentences.json")
print("prepare valid sentences file...")
finale_sentences(valid_final_sentences, address + "/valid_sentences.json")
stop = time.time()
print("Execution time: ", stop - start)

