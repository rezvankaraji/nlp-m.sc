import matplotlib.pyplot as plt
from math import log10


def plot_power_law(words_freq, n):
    x = []
    y = []
    most_frequents = sorted_words_freq = sorted(
        words_freq, key=words_freq.get, reverse=True
    )
    most_frequents = most_frequents[:n]
    for word in most_frequents:
        x.append(log10(words_freq[word]))
        y.append(log10(most_frequents.index(word) + 1))

    plt.plot(x, y)
    plt.show()

