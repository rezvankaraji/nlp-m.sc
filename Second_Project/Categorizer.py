import Normalizer
import Tokenizer
import Vectorizer
import Model
import Trainer


class Categorizer:
    def __init__(self, directory, level):
        self.directory = directory
        self.level = level

    def clean(self):
        normalizer = Normalizer.Normalizer(self.directory)
        # process train and valid data
        normalizer.begin_process(file_name="train.csv")
        train_data, valid_data = normalizer.get_clean_data()
        # process test data
        normalizer.begin_process(file_name="test.csv", is_test=True)
        test_data = normalizer.get_clean_data(is_test=True)

        return train_data, valid_data, test_data

    def tokenize(self, data):
        train_data, valid_data, test_data = data

        tokenizer = Tokenizer.Tokenizer(self.directory)

        if self.level == 0:  # word
            return (
                tokenizer.word_level(train_data),
                tokenizer.word_level(valid_data),
                tokenizer.word_level(test_data),
            )

        elif self.level == 1:  # char
            return (
                tokenizer.char_level(train_data),
                tokenizer.char_level(valid_data),
                tokenizer.char_level(test_data),
            )

        elif self.level == 2:  # both
            train_data = tokenizer.word_level(train_data)
            valid_data = tokenizer.word_level(valid_data)
            test_data = tokenizer.word_level(test_data)
            return (
                tokenizer.char_level(train_data),
                tokenizer.char_level(valid_data),
                tokenizer.char_level(test_data),
            )

    def vectorize(self, data):
        train_data, valid_data, test_data = data

        vectorizer = Vectorizer.Vectorizer(self.directory)

        if self.level == 0:  # word
            return (
                vectorizer.TF_IDF(train_data, key="text"),
                vectorizer.TF_IDF(valid_data, key="text"),
                vectorizer.TF_IDF(test_data, key="text"),
            )

        elif self.level == 1:  # char
            return (
                vectorizer.TF_IDF(train_data, key="chars"),
                vectorizer.TF_IDF(valid_data, key="chars"),
                vectorizer.TF_IDF(test_data, key="chars"),
            )

        elif self.level == 2:  # both
            train_data = vectorizer.TF_IDF(train_data, key="text")
            valid_data = vectorizer.TF_IDF(valid_data, key="text")
            test_data = vectorizer.TF_IDF(test_data, key="text")
            return (
                vectorizer.TF_IDF(train_data, key="chars"),
                vectorizer.TF_IDF(valid_data, key="chars"),
                vectorizer.TF_IDF(test_data, key="chars"),
            )

    def defining_model(self, model_name, data):
        level = self.level
        model = Model.Model("category", level)

        if model_name == "naive bayes":
            model.naive_bayes(data)
            return model

        elif model_name == "lstm":
            model.LSTM(data)
            return model

    def train(self, model_name, model):
        trainer = Trainer.Trainer(model)

        if model_name == "naive bayes":
            trainer.naive_bayes()
            return trainer

        elif model_name == "lstm":
            trainer.LSTM()
            return trainer

