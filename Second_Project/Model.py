from sklearn.naive_bayes import MultinomialNB


class Model:
    def __init__(self, y_name, level):
        self.y_name = y_name
        self.level = level

    def naive_bayes(self, data):
        train_data, valid_data, test_data = data
        self.train_X, self.train_y = self.prepare_inputs(train_data)
        self.test_X, self.test_y = self.prepare_inputs(test_data)
        self.nb = MultinomialNB()

    def prepare_inputs(self, data):
        y = set()
        X = []
        for news in data:
            Y.add(news[self.y_name])
            del news[self.y_name]

            if self.level == 0:  # word
                X.append(news["text"])
            elif self.level == 1:  # char
                X.append(news["chars"])
            elif self.level == 2:  # both
                x = news["words"]
                for char in news["chars"]:
                    x.append(char)
                X.append(x)

    def LSTM(self, data):
        train_data, valid_data, test_data = data

        self.partition = {}
        self.lables = {}
        self.patition_and_labels(train_data, "train")
        self.patition_and_labels(valid_data, "valid")

        self.MAX_NB_WORDS = 10000
        self.EMBEDDING_DIM = 100
        lstm = Sequential()
        lstm.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=X.shape[1]))
        lstm.add(SpatialDropout1D(0.2))
        lstm.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
        lstm.add(Dense(len(y), activation="softmax"))
        lstm.compile()

        self.lstm = lstm
        self.params = {
            "dim": (32, 32, 32),
            "batch_size": 64,
            "n_classes": 6,
            "n_channels": 1,
        }

    def patition_and_labels(self, data, type_name):
        for news in data:
            self.list_IDs.append(news["title"])
            self.partition[name].append(news["title"])
            self.lables[news["title"]] = news[self.y_name]

