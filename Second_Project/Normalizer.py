import re
import hazm
import sys
import csv
import random

csv.field_size_limit(sys.maxsize)


class Normalizer:
    def __init__(self, directory):
        self.directory = directory

    def begin_process(self, file_name, is_test=False):

        # read csv file
        data = []
        address = self.directory + file_name
        with open(address, "r", encoding="utf-8") as raw_file:
            data = list(
                csv.DictReader(raw_file, delimiter="\t")
            )  # data[0] == ['link', 'title', 'code_news', 'category', 'date', 'text']
        # normalize text of the news
        data = data[1:]  # a list of dictionaries

        # remove information other than text, title, category
        new_data = []
        for news in data:
            new_news = {
                "category": news["category"],
                "title": news["title"],
                "text": news["text"],
            }
            new_data.append(new_news)

        if not is_test:
            print("spliting train data into train and valid sets ...")
            valid_data, train_data = self.data_spliter(new_data)
            print("process train data ...")
            self.train_data = self.process(train_data, data_type="train")
            print("process valid data ...")
            self.valid_data = self.process(valid_data, data_type="valid")
        else:
            print("process test data ...")
            self.test_data = self.process(new_data, data_type="test")

    # calls other functions in order to process data
    def process(self, data, data_type):
        news_lengths = []
        for news in data:
            # normalize
            news["text"] = self.normalize(news["text"])

            # word tokenize
            news["text"] = self.word_tokenizer(news["text"])
            news_lengths.append(len(news["text"]))

            # stemmer
            news["text"] = self.stem(news["text"])

            # eliminate stopwords
            news["text"] = self.drop_stop_words(news["text"])

        if data_type == "train":
            # calculate the  average size of the news
            sum = 0
            for length in news_lengths:
                sum += length
            self.average = sum // len(news_lengths)

        # drop news that are longer than the average
        count = 0
        for index, value in enumerate(news_lengths):
            if value > self.average:
                data.remove(data[index - count])
                count += 1

        # calculate frequencies for every word and number of all words
        total_words, words_freq = self.calc_freq(data)
        print(
            "Number of words : ",
            total_words,
            "  and Number of distinct words : ",
            len(words_freq),
        )

        # obtain 10000 most frequent words and save it in most_frequent.txt
        if data_type == "train":
            self.frequent_words = self.top_n_frequents(words_freq, total_words, 10000)
            self.save(self.frequent_words, name="most_frequent")

        # replace non-frequent words with UNK
        data = self.replace_nonfrequent(data)

        # char tokenize
        sum = 0
        max_char_size = 0
        for news in data:
            news["chars"] = self.char_tokenizer(news["text"])
            sum += len(news["chars"])
            if max_char_size < len(news["chars"]):
                max_char_size = len(news["chars"])
        print("number of characters: ", sum)

        # same-sizing words and chars
        for news in data:
            # word
            diff = self.average - len(news["text"])
            for i in range(diff):
                news["text"].append("PAD")

            # char
            diff = max_char_size - len(news["chars"])
            for i in range(diff):
                news["chars"].append("PAD")

        # obtain percentile of the coverage of the text by the frequent words
        coverage = self.coverage_percent(words_freq, total_words)
        print("Coverage of the text by the train frequent words : ", coverage, "%")

        return data

    # splits data into train set and validation set
    def data_spliter(self, data):
        random.shuffle(data)
        valid_size = len(data) * 30 // 100
        valid_data = data[:valid_size]
        train_data = data[valid_size:]
        return valid_data, train_data

    # remove every extra characters except . and ? and persian words and numbers and replace every number in text with 'N'.
    def normalize(self, text):
        normal_text1 = re.sub(r"[a-z]", " ", text, flags=re.I)
        normal_text2 = re.sub(r"[__ـ_ء]", " ", normal_text1)
        normal_text3 = re.sub(r"[^\w\s . ?]", " ", normal_text2)
        normal_text4 = re.sub(r"[1234567890۱۲۳۴۵۶۷۸۹۰١٢٣٤٥٦٧٨٩٠]+", " N ", normal_text3)
        normal_text5 = re.sub(r"[ئيی]", "ى", normal_text4)
        normal_text6 = re.sub(r"[ك]", "ک", normal_text5)
        normal_text7 = re.sub(r"[ؤ]", "و", normal_text6)
        normal_text8 = re.sub(r"[ة]", "ه", normal_text7)
        normal_text9 = re.sub(r"[أإ]", "ا", normal_text8)
        normal_text = re.sub(r"[^\S\n\t]+", " ", normal_text9)
        return normal_text

    def word_tokenizer(self, text):
        text = re.sub(r"[.?]", " ", text)
        words = hazm.word_tokenize(text)
        for word in words:
            word = re.sub(r"[n]", "N", word)
        return words

    def char_tokenizer(self, words):
        chars = []
        for word in words:
            if word == "UNK":
                chars.append("UNK")
            else:
                for char in word:
                    chars.append(char)
        return chars

    def calc_freq(self, data):
        words_freq = {}
        seen_word = set()
        total_words = 0
        for news in data:
            for word in news["text"]:
                if word not in seen_word:
                    words_freq[word] = 1
                    seen_word.add(word)
                else:
                    words_freq[word] += 1
                total_words += 1
        return total_words, words_freq

    # list n most frequent words in most_frequent.txt
    def top_n_frequents(self, words_freq, total_words, n):
        # sort frequencies
        frequent_words = sorted(words_freq, key=words_freq.get, reverse=True)
        frequent_words = set(frequent_words[:n])
        return frequent_words

    # calc frequent words coverage percentile
    def coverage_percent(self, words_freq, total_words):
        # frequent_percent
        sum = 0
        for word in words_freq:
            if word in self.frequent_words:
                sum += words_freq[word]
        percentile = (sum / total_words) * 100
        return percentile

    # replace non-frequent words with UNK
    def replace_nonfrequent(self, data):
        for news in data:
            new_text = []
            for word in news["text"]:
                if not (word == "N" or word in self.frequent_words):
                    word = "UNK"
                new_text.append(word)
            news["text"] = new_text
        return data

    def save(self, data, name):
        address = self.directory + name + ".txt"
        with open(address, "w", encoding="utf-8") as txt_file:
            for word in data:
                txt_file.write(word)
                txt_file.write("\n")

    def get_clean_data(self, is_test=False):
        if is_test:
            return self.test_data
        else:
            return self.train_data, self.valid_data

    def stem(self, words):
        stemmer = hazm.Stemmer()
        new_words = []
        for word in words:
            new_words.append(stemmer.stem(word))
        return new_words

    def drop_stop_words(self, words):
        stop_words = []
        address = self.directory + "stop_words.txt"
        with open(address, "r", encoding="utf-8") as txt_file:
            stop_words = set(txt_file.read().split("\n"))
        new_words = []
        for word in words:
            if word not in stop_words:
                new_words.append(word)
        return new_words
