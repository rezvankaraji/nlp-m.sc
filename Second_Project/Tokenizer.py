import pickle


class Tokenizer:
    def __init__(self, directory):
        self.directory = directory
        address = self.directory + "most_frequent.txt"
        most_frequents = []
        with open(address, "r", encoding="utf-8") as txt_file:
            self.most_frequents = txt_file.read().split("\n")

    def obtain_chars(self, words):
        chars = set()
        for word in words:
            for char in word:
                if char not in chars:
                    chars.add(char)
        return list(chars)

    def map(self, items):
        items.append("PAD")
        items.append("UNK")
        index_to_value = {}
        for index, value in enumerate(items):
            index_to_value[index] = value
        value_to_index = {}
        for index, value in enumerate(items):
            value_to_index[value] = index
        return value_to_index, index_to_value

    def word_level(self, data):
        words = sorted(self.most_frequents)
        word2index, index2word = self.map(words)
        self.save("word2index", word2index)
        self.save("index2word", index2word)

        for news in data:
            news_vector = []
            for word in news["text"]:
                news_vector.append(word2index[word])

            news["text"] = news_vector
        return data

    def char_level(self, data):
        chars = self.obtain_chars(self.most_frequents)
        chars = sorted(chars)
        # ['N', '_', 'آ', 'ا', 'ب', 'ت', 'ث', 'ج', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ل', 'م', 'ن', 'ه', 'و', 'ى', 'پ', 'چ', 'ژ', 'ک', 'گ']
        char2index, index2char = self.map(chars)
        self.save("char2index", char2index)
        self.save("index2char", index2char)

        for news in data:
            news_vector = []
            for char in news["chars"]:
                news_vector.append(char2index[char])

            news["chars"] = news_vector
        return data

    def save(self, name, data):
        address = self.directory + name + ".pickle"
        with open(address, "wb") as pfile:
            pickle.dump(data, pfile)
