import numpy as np

from keras.models import Sequential
import DataGenerator


class Trainer:
    def __init__(self, model):
        self.model = model

    def naive_bayes(self):
        X = self.model.train_X
        y = self.model.train_y
        self.model.nb.fit(X, y)

    def LSTM(self):
        params = model.params

        train_generator = DataGenerator.DataGenerator(
            self.model.partition["train"], self.model.labels, **params
        )
        valid_generator = DataGenerator.DataGenerator(
            self.model.partition["valid"], self.model.labels, **params
        )

        self.model.lstm.fit_generator(
            generator=train_generator,
            validation_data=valid_generator,
            use_multiprocessing=True,
            workers=6,
        )

