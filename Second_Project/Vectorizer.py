import math


class Vectorizer:
    def __init__(self, directory):
        self.directory = directory

    def TF_IDF(self, data, key):
        freq, freq_in_news = self.frequency(data, key)
        number_of_news = len(data)
        number_of_items = len(freq)
        tf_idf = [[0 for c in range(number_of_items)] for r in range(number_of_news)]

        for x in freq_in_news:
            item, news_index = x

            tf = freq_in_news[x]
            df = freq[item]
            w = tf * math.log2(number_of_news / df)
            tf_idf[news_index][item] = w

        news_index = 0
        for news in data:
            news[key] = tf_idf[news_index]
            news_index += 1

        return data

    def frequency(self, data, key):
        # item is either WORD or CHARACTER
        freq = []  #  number of news containing each item
        freq_in_news = {}  # (item, news_index) : frequency
        seen_tuples = set()  # (item, news_index)
        news_index = 0
        for news in data:
            seen_items = set()
            for item in news[key]:
                if item not in seen_items:
                    while item >= len(freq):
                        freq.append(0)
                    freq[item] += 1
                    seen_items.add(item)

                tuple_key = (item, news_index)
                if tuple_key in seen_tuples:
                    freq_in_news[tuple_key] += 1
                else:
                    freq_in_news[tuple_key] = 1
                    seen_tuples.add(tuple_key)

            news_index += 1

        return freq, freq_in_news

