import Categorizer
import time

directory = ""

categorizer = Categorizer.Categorizer(directory=directory, level=2)
start = time.time()
cleaned_data = categorizer.clean()
stop = time.time()
print("clean in: ", stop - start)

start = time.time()
tokenized_data = categorizer.tokenize(cleaned_data)
stop = time.time()
print("tokenize in: ", stop - start)

start = time.time()
nb_model = categorizer.defining_model("naive bayes", tokenized_data)
stop = time.time()
print("nb_model in: ", stop - start)

start = time.time()
nb_trained = categorizer.train("naive bayes", nb_model)
stop = time.time()
print("nb_train in: ", stop - start)

start = time.time()
vectorized_data = categorizer.vectorize(tokenized_data)
stop = time.time()
print("vectorize in: ", stop - start)

start = time.time()
lstm_model = categorizer.defining_model("lstm", vectorized_data)
stop = time.time()
print("lstm_model in: ", stop - start)

start = time.time()
lstm_trained = categorizer.train("lstm", lstm_model)
stop = time.time()
print("lstm_train in: ", stop - start)

